<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

  <?php

    // --Основы ООП--
    class Cat {
      private $name;
      public $color;
      public $sound;

      // Методы
      // Задаёт обязательный аргумент для создания объекта
      public function __construct(string $name) 
      {
        $this->name = $name;
      }
      public function sayHello() 
      {
        echo 'Привет, меня зовут '.$this->name.'.';
      }
      // Сеттер
      public function setName(string $name) 
      {
        $this->name = $name;
      }
      // Геттер
      public function getName(): string 
      {
        return $this->name;
      }         
    }
    $cat1 = new Cat('Murka');
    
    // Получаем доступ к свойствам объекта
    // $cat1->color = "black";
    // $cat1->sound = "mewwww";

    // $cat1->sayHello();
    // echo $cat1->getName();


    // --Наследование--
    class Post {
      // Свойства private будут недоступны дочерним классам
      // private $title;
      protected $title;
      protected $text;

      public function __construct(string $title, string $text) 
      {
        $this->title = $title;
        $this->text = $text;
      }

      public function getTitle()
      {
        return $this->title;
      }

      public function getText()
      {
        return $this->text;
      }

      public function setText($text): void
      {
        $this->text = $text;
      }
    }
    // Наследование методов и свойств другого класса 
    class Lesson extends Post {
        private $homework;

        public function __construct(string $title, string $text, string $homework) 
        {
          // Наследование части метода
          parent::__construct($title, $text);
          $this->homework = $homework;
        }

        public function getHomework(): string 
        {
          return $this->homework;
        }

        public function setHomework(string $homework): void
        {
          $this->homework = $homework;
        }
    }

    $lesson = new Lesson('Title', 'Text', 'Homework');


    // --Интерфейсы--
    interface CalculateSquare
    {
      public function calculateSquare(): float;
    }

    class Rectangle
    {
      private $x;
      private $y;

      public function __construct(float $x, float $y)
      {
        $this->x = $x;
        $this->y = $y;
      }

      public function calculateSquare(): float
      {
        return $this->x * $this->y;
      }
    }

    class Square implements CalculateSquare
    {
      private $x;

      public function __construct(float $x)
      {
        $this->x = $x;
      }

      public function calculateSquare(): float
      {
        return $this->x ** 2;
      }
    }

    class Circle implements CalculateSquare
    {
      // Константа класса
      const PI = 3.1416;
      private $r;

      public function __construct(float $r)
      {
        $this->r = $r;

      }

      public function calculateSquare(): float
      {
        return self::PI * ($this->r ** 2);
      }
    }

    

    $circle1 = new Circle(2.5);
    // Проверка принадлежность к классу
    // var_dump($circle1 instanceof Rectangle);
    // Проверка принадлежность к интерфейсу
    // var_dump($circle1 instanceof CalculateSquare);
    $objects = [
      new Square(5),
      new Rectangle(3, 8),
      new Circle(3)
    ];

    foreach ($objects as $object) {
      if ($object instanceof CalculateSquare) {
        echo 'Объект реализует интерфейс CalculateSquare. Площадь: '.$object->calculateSquare();
        echo '<br>';
      }
    }


    // --Абстрактные классы--
    abstract class AbstractClass
    {
      abstract public function getValue();
      
      public function printValue()
      {
        echo 'Значение: '.$this->getValue();
      }
    }

    class ClassA extends AbstractClass
    {
      private $value;

      public function __construct(string $value)
      {
        $this->value = $value;
      }

      public function getValue()
      {
        return $this->value;
      }
    }

    $objA = new ClassA('kek');
    $objA->printValue();


    // --Статические свойства и методы--
    class A {
      public static function test(int $x)
      {
        return 'x= '.$x;
      }
    }
    // Можно использовать, не создавая объекты
    // echo A::test(5);

    class UserA 
    {
      private $role;
      private $name;

      public function __construct(string $role, string $name)
      {
        $this->role = $role;
        $this->name = $name;
      }

      public static function createAdmin(string $name)
      {
        return new self('admin', $name);
      }
    }
    $admin = UserA::createAdmin('Ivan');
    // var_dump($admin);

    class Human
    {
      private static $count = 0;

      // Функция прибавляет 1 при создании нового объекта
      public function __construct() 
      {
        self::$count++;
      }

      public static function getCount()
      {
        return self::$count;
      }
    }

    $human1 = new Human();
    $human2 = new Human();
    echo 'Людей уже '.Human::getCount();


    // --Объектно-ориентированный подход--
    class User 
    {
      private $name;

      public function __construct(string $name)
      {
        $this->name = $name;
      }

      public function getName(): string
      {
        return $this->name;
      }
    }

    class Article {
      private $title;
      private $text;
      private $author;

      // Скрытая типизация
      public function __construct(string $title, string $text, User $author) 
      {
        $this->title = $title;
        $this->text = $text;
        $this->author = $author;
      }

      public function getTitle()
      {
        return $this->title;
      }

      public function getText()
      {
        return $this->text;
      }

      public function getAuthor(): User
      {
        return $this->author;
      }
    }

    $author = new User('Ivan');
    $article = new Article('В мире животных', 'Lorem ipsum', $author);

    echo 'Имя автора: '.$article->getAuthor()->getName();



    // ------Домашняя работа--------
    // Задание 1
    abstract class HumanAbstract
    {
      private $name;

      public function __construct(string $name)
      {
          $this->name = $name;
      }

      public function getName(): string
      {
          return $this->name;
      }

      abstract public function getGreetings(): string;
      abstract public function getMyNameIs(): string;

      public function introduceYourself(): string
      {
          return $this->getGreetings() . '! ' . $this->getMyNameIs() . ' ' . $this->getName() . '.';
      }
    }

    class RussianHuman extends HumanAbstract
    {
      public function getGreetings(): string
      {
        return 'Привет';
      }

      public function getMyNameIs(): string
      {
        return 'Меня зовут ';
      }
    }

    class EnglishHuman extends HumanAbstract
    {
      public function getGreetings(): string
      {
        return 'Hi';
      }

      public function getMyNameIs(): string
      {
        return 'My name is ';
      }
    }

    $russian = new RussianHuman('Иван');
    $english = new EnglishHuman('John');

    echo $russian->introduceYourself();
    echo $english->introduceYourself();

    // Задание 2
    class Cats {
      private $name;
      private $color;
      public $sound;

      public function __construct(string $name, string $color) 
      {
        $this->name = $name;
        $this->color = $color;
      }
      public function sayHello() 
      {
        echo 'Привет, меня зовут '.$this->name.', мой цвет ', $this->color.'<br>';
      }
      public function setName(string $name) 
      {
        $this->name = $name;
      }
      public function getName(): string 
      {
        return $this->name;
      }   
      public function getColor(): string 
      {
        return $this->color;
      }        
    }

    $cat1 = new Cats('Барсик','белый');
    $cat1->sayHello();
    echo $cat1->getColor();

    // Задание 3
    class PaidLesson extends Lesson
    {
      private $price;

      public function __construct(string $title, string $text, string $homework, float $price) 
      {
        parent::__construct($title, $text, $homework);
        $this->price = $price;
      }
      public function setPrice(int $price)
      {
        $this->price = $price;
      }
      public function getPrice(int $price)
      {
        return $this->price;
      }
    }

    $paidlesson1 = new PaidLesson('Урок о наследовании в PHP', 'Лол, кек, чебурек', 'Ложитесь спать, утро вечера мудренее', 99.90);
    var_dump($paidlesson1);

    // Задание 4
    interface CalculateSquare1
    {
      public function calculateSquare(): float;
    }

    class Rectangle1
    {
      private $x;
      private $y;

      public function __construct(float $x, float $y)
      {
        $this->x = $x;
        $this->y = $y;
      }

      public function calculateSquare(): float
      {
        return $this->x * $this->y;
      }
      public function getClass()
      {
          return get_class($this);
      }
    }

    class Square1 implements CalculateSquare
    {
      private $x;

      public function __construct(float $x)
      {
        $this->x = $x;
      }

      public function calculateSquare(): float
      {
        return $this->x ** 2;
      }
      public function getClass()
      {
          return get_class($this);
      }
    }

    class Circle1 implements CalculateSquare
    {
      // Константа класса
      const PI = 3.1416;
      private $r;

      public function __construct(float $r)
      {
        $this->r = $r;

      }

      public function calculateSquare(): float
      {
        return self::PI * ($this->r ** 2);
      }
      public function getClass()
      {
          return get_class($this);
      }
    }

    $objects = [
      new Square1(5),
      new Rectangle1(3, 8),
      new Circle1(3)
    ];

    foreach ($objects as $object) {
      if ($object instanceof CalculateSquare) 
      {
        echo 'Объект реализует интерфейс CalculateSquare. Площадь: '.$object->calculateSquare();
        echo 'Этот объект принадлежит классу '.$object->getClass();
        echo '<br>';
      }
      else 
      {
        echo 'Объект класса '.$object->getClass().'не реализует интерфейс CalculateSquare.<br>';
      }
    }
    
  ?>
</body>
</html>