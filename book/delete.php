<?php
    $database = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_1715_db', 'wjF1gXRhFp6z', 'std_1715_db');
    if (mysqli_connect_error()) echo '<div class="error">Ошибка подключения к базе данных..</div>';

    if (isset($_GET['id'])) {
        $sql = "DELETE FROM `notebook` WHERE `id` = " . $_GET['id'];
        $res = mysqli_query($database, $sql);
        if (mysqli_errno($database)) echo '<div class="error">Ошибка запроса</div>';
        else echo '<div class="success">Запись удалена</div>';
    }

    $sql = 'SELECT `id`, `surname`, LEFT(`name`, 1) AS `name`, LEFT(`lastname`, 1) AS `lastname` FROM `notebook`';
    $res = mysqli_query($database, $sql);

    if (mysqli_errno($database)) echo '<div class="error">Ошибка запроса</div>';
    if (!mysqli_num_rows($res)) {
        echo '<div class="success">В книжке отсутствуют записи</div>';
        exit;
    }

    echo '<div class="users">';
    while ($user = mysqli_fetch_assoc($res)) {
        echo '<a class="users__item" href="?page=delete&id=' . $user['id'] . '">' . $user['surname'] . ' ' . $user['name'] . '.' . $user['lastname'] . '.</a>'; 
    }
    echo '</div>';
?>