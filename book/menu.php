<?php
    echo '<header>';

    if (!isset($_GET['page'])) $_GET['page'] = 'view';

    echo '<a href="?page=view" ' .  ($_GET['page'] == 'view' ? 'class="select"' : '') . '>Просмотр</a>';
    echo '<a href="?page=add" ' .  ($_GET['page'] == 'add' ? 'class="select"' : '') . '>Добавление записи</a>';
    echo '<a href="?page=edit" ' .  ($_GET['page'] == 'edit' ? 'class="select"' : '') . '>Редактирование записи</a>';
    echo '<a href="?page=delete" ' .  ($_GET['page'] == 'delete' ? 'class="select"' : '') . '>Удаление записи</a>';

    echo '</header>';

    if ($_GET['page'] == 'view') {
        $sorts = array(
            'byid' => "По умолчанию",
            "byname" => "По фамилии"
        );
    
        echo '<div class="submenu">';

        if (!isset($_GET['sort'])) $_GET['sort'] = 'byid';

        echo '<a href="?page=view&sort=byid"' . ($_GET['sort'] == 'byid' ? 'class="select"' : '') . '>По умолчанию</a>';
        echo '<a href="?page=view&sort=byname"' . ($_GET['sort'] == 'byname' ? 'class="select"' : '') . '>По фамилии</a>';
        
        echo '</div>';
    }
?>