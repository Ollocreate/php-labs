<?php

    function getFriendList() {
        $database = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_1715_db', 'wjF1gXRhFp6z', 'std_1715_db');
        if (mysqli_connect_error()) echo '<div class="error">Ошибка подключения к базе данных..</div>';

        $sql = 'SELECT * FROM `notebook`';
        $res = mysqli_query($database, $sql);

        if (!mysqli_num_rows($res)) {
            return '<div class="error">В книжке отсутствуют записи</div>';
        }

        $num_pages = floor(mysqli_num_rows($res) / 10);

        if (!isset($_GET['pos'])) $_GET['pos'] = 0;

        $sql = 'SELECT * FROM `notebook`';
        if ($_GET['sort'] == 'byname') $sql .= 'ORDER BY `surname`';
        $sql .= ' LIMIT ' . $_GET['pos'] * 10 . ', 10';
        $res = mysqli_query($database, $sql);

        $html = '<table><thead><tr><th>ID</th><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Пол</th><th>Дата Рождения</th><th>Телефон</th><th>Адрес</th><th>Email</th><th>Комментарий</th>';
        $html .= '</tr></thead><tbody>';
        
        while ($user = mysqli_fetch_assoc($res)) {
            $html .= '<tr class="table__item">';
            $html .= '<td>' . $user['id'] . '</td><td>' . $user['surname'] . '</td><td>' . $user['name'] . '</td><td>' . $user['lastname'] . '</td><td>' . $user['gender'] . '</td>';
            $html .= '<td>' . $user['date'] . '</td><td>' . $user['phone'] . '</td><td>' . $user['location'] . '</td><td>' . $user['email'] . '</td><td>' . $user['comment'] . '</td>';
            $html .= '</tr>';
        }

        $html .= '</tbody></table>';

        if ($num_pages) {
            $html .= '<div class="pagination">';
        
            for ($i = 0; $i <= $num_pages; $i++) {
                $html .= '<a ';
                if ($_GET['pos'] != $i) $html .= 'href="?page=view&pos=' . $i . '"';
                $html .= '>' . ($i + 1) . '</a>';
            }

            $html .= '</div>';
        }

        return $html;
    }

?>