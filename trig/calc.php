<?php

require "trig.php";

function checkValidation(string $str) {
    if (preg_match("~[0-9]+\(~", $str) || preg_match("~\)[0-9]+]~", $str)) error("Ошибка со скобками..");
    if (preg_match("~[+*/]{2,}~", $str)) error("Ошибка операций..");
    if (preg_match("~/0~", $str)) error("Деление на ноль..");
    if (preg_match("~[^0-9]+\.~", $str) || preg_match("~\.[^0-9]+~", $str) || preg_match("~^\.~", $str)) error("Странная дробь..");
    if (preg_match("~[0-9]+(?:\.[0-9]+){2,}~", $str)) error("Странная дробь..");
    if (substr_count($str, "(") != substr_count($str, ")")) error("Ошибка со скобками..");


    $brackets = 0;
    for ($i = 0; $i < strlen($str); $i++) {
        if ($str[$i] == "(") $brackets++;
        if ($str[$i] == ")") $brackets--;

        if ($brackets < 0) error("Ошибка со скобками..");
    }
    if ($brackets) error("Ошибка со скобками..");

}

function error(string $message) {
    header('Location: index.php?q=' . $message . '&t=error');
    exit;
}

function addition($arr) {
    $numbers = explode('+', $arr[1]);
    return (double)$numbers[0] + (double)$numbers[1];
}

function multiplication($arr) {
    $numbers = explode('*', $arr[1]);
    return (double)$numbers[0] * (double)$numbers[1];
}

function division($arr) {
    $numbers = explode('/', $arr[1]);
    if ($numbers[1] == 0) error("Деление на ноль");
    return (double)$numbers[0] / (double)$numbers[1];
}

function calc(string $str) {
    $str = preg_replace("~--~", "+", $str);
    $str = preg_replace("~([0-9]+)-~", "$1+-", $str);

    while (strpos($str, "*") !== false) {
        $str = preg_replace_callback("~(-?[0-9.]+\*-?[0-9.]+)~", 'multiplication', $str);
    }

    while (strpos($str, "/") !== false) {
        $str = preg_replace_callback("~(-?[0-9.]+\/-?[0-9.]+)~", 'division', $str);
    }

    while (strpos($str, "+") !== false) {
        $str = preg_replace_callback("~(-?[0-9.]+\+-?[0-9.]+)~", 'addition', $str);
    }

    return $str;
}

function deleteBrackets($arr) {
    $str = $arr[1];
    return calc($str);
}

function calculating(string $str) {
    while (strpos($str, "(") !== false) {
        $str = preg_replace_callback("~\(([^\(\)]*)\)~", 'deleteBrackets', $str);
    }

    return $str;
}

if (isset($_POST['submit'])) {
    $value = $_POST['calc-value'];

    $isTrig = false;
    foreach($trigFunctions as $trigFunction) {
        if (strpos($value, $trigFunction) !== false) $isTrig = true;
    }

    if ($isTrig) {
        trigValidation($value);
        $value = trig($value);
    }

    checkValidation($value);
    $result = calculating('(' . $value . ')');

    header('Location: index.php?q=' . $result . '');
}