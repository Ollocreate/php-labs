const input = document.querySelector('.calculator__input');

const changeSizeInput = () => {
    input.style.fontSize = '48px';

    if (input.value.length > 12) {
        input.style.fontSize = '36px';
    }

    if (input.value.length > 16) {
        input.style.fontSize = '24px';
    }

    if (input.value.length > 26) {
        input.style.fontSize = '20px';
    }
}

changeSizeInput();

document.querySelectorAll('.calculator__button[data-type="char"]').forEach(element => element.addEventListener('click', event => {
    event.preventDefault();
    if (input.dataset.update == "true") {
        input.value = "";
        input.dataset.update = "false";
    }
    input.value += event.currentTarget.textContent;
}));

document.querySelectorAll('.calculator__button[data-type="trig"]').forEach(element => element.addEventListener('click', event => {
    event.preventDefault();
    if (input.dataset.update == "true") {
        input.value = "";
        input.dataset.update = "false";
    }
    input.value += event.currentTarget.dataset.value + "(";
}));

document.querySelector('.calculator__button[data-type="clear"').addEventListener('click', event => {
    event.preventDefault();
    input.value = "";
});

document.querySelector('.calculator__button[data-type="delete"').addEventListener('click', event => {
    event.preventDefault();
    if (input.value[input.value.length - 1] == "(" && ['s', 'n'].includes(input.value[input.value.length - 2])) {
        input.value = input.value.slice(0, -4);
    } else {
        input.value = input.value.slice(0, -1);
    }
});


document.querySelectorAll('.calculator__button').forEach(element => element.addEventListener('click', changeSizeInput));

