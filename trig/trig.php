<?php

$trigFunctions = array("cos", "sin", "tan");

function trigValidation(string $str) {
    if (preg_match("~(?:cos|sin|tan)\(\)~", $str)) error("Ошибка с тригонометрической функцией..");
}

function calcTrig($arr) {
    $operation = $arr[1];
    $str = $arr[2];
    return $operation(calculating($str));
}

function trig(string $str) {
    $str = preg_replace_callback("~(cos|sin|tan)(\([0-9+\-/*]+\))~", "calcTrig", $str);
    return $str;
}

$trigData = file_get_contents("expression.txt");
$trigResult = calculating("(" . trig($trigData) . ")");
file_put_contents("result.txt", $trigResult);
