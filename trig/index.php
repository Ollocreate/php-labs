<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Калькулятор</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php if (!isset($_GET['q'])) $_GET['q'] = "";
        if (!isset($_GET['t'])) $_GET['t'] = "" ?>
<main>
    <div class="container">
        <form action="calc.php" method="post" class="calculator">
            <div class="calculator__preview">
                <input readonly type="text" class="calculator__input" name="calc-value" data-update="<?= $_GET['t'] == 'error' ? 'true' : 'false'?>" value="<?= $_GET['q']; ?>">
            </div>
            <div class="calculator__buttons">
                <button class="calculator__button" data-type="delete"><</button>
                <button class="calculator__button" data-type="clear">C</button>
                <button class="calculator__button" data-type="char">(</button>
                <button class="calculator__button" data-type="char">)</button>
                <button class="calculator__button calculator__button--yellow" data-type="char">/</button>
                <button class="calculator__button" data-type="trig" data-value="sin">sin</button>
                <button class="calculator__button" data-type="char">7</button>
                <button class="calculator__button" data-type="char">8</button>
                <button class="calculator__button" data-type="char">9</button>
                <button class="calculator__button calculator__button--yellow" data-type="char">*</button>
                <button class="calculator__button" data-type="trig" data-value="cos">cos</button>
                <button class="calculator__button" data-type="char">4</button>
                <button class="calculator__button" data-type="char">5</button>
                <button class="calculator__button" data-type="char">6</button>
                <button class="calculator__button calculator__button--yellow" data-type="char">-</button>
                <button class="calculator__button" data-type="trig" data-value="tan">tg</button>
                <button class="calculator__button" data-type="char">1</button>
                <button class="calculator__button" data-type="char">2</button>
                <button class="calculator__button" data-type="char">3</button>
                <button class="calculator__button calculator__button--yellow" data-type="char">+</button>
                <button class="calculator__button" data-type="trig" data-value="1/tg">ctg</button>
                <button class="calculator__button calculator__button--big" data-type="char">0</button>
                <button class="calculator__button" data-type="char">.</button>
                <button class="calculator__button calculator__button--yellow" name="submit" data-type="calc">=</button>
            </div>
        </form>
    </div>
</main>
<script src="main.js"></script>
</body>
</html>