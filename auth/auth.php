<?php

session_start();

$database = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_1808_neustroeva', 'neustroeva(123)', 'std_1808_neustroeva');
if (mysqli_connect_error()) {
    $_SESSION['error'] = 'connection';
    header('Location: authorization.php');
}

if (isset($_POST['auth'])) {
    $password = trim(htmlspecialchars($_POST['password']));
    $login = trim(htmlspecialchars($_POST['login']));

    if (!strlen($login) || !strlen($password)) {
        $_SESSION['error'] = 'data';
        header('Location: authorization.php');
    }

    $sql = 'SELECT `id`, `password` FROM users WHERE `email` = "' . $login . '";';
    $res = mysqli_query($database, $sql);
    if (mysqli_errno($database)) {
        $_SESSION['error'] = 'connection';
        header('Location: authorization.php');
    }

    $res = mysqli_fetch_assoc($res);

    if ($res) {
        if (md5($password) != $res['password']) {
            $_SESSION['error'] = 'password';
            header('Location: authorization.php');
        }
        else {
            $_SESSION['UID'] = $res['id'];

            if ($_POST['save-me'] == "on") {
                $token = $res['id'] . $res['email'] . $res['password'];
                $token = md5($token);
                $sql = 'UPDATE `users` SET `token` = "' . $token . '" WHERE `id` = ' . $res['id'] . ';';

                mysqli_query($database, $sql);
                if (mysqli_errno($database)) {
                    $_SESSION['error'] = 'connection';
                    header('Location: authorization.php');
                }

                setcookie('token', $token, time() + 604800);
            } else {
                setcookie('token', '', time() - 3600);
            }

            header('Location: index.php');
        }
    } else {
        $_SESSION['error'] = 'user';
    }
}
header('Location: authorization.php');


?>