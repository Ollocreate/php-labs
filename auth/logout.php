<?php
session_start();
unset($_SESSION['UID']);
setcookie('token', "", time() - 3600);
header('Location: authorization.php');