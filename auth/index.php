<?php

session_start();

if (!isset($_SESSION['UID'])) header('Location: authorization.php');

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная страница</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <main class="view">
        <h1 class="view__title">Вы авторизованы</h1>
        <div class="view__buttons">
            <a href="logout.php" class="view__button button button--danger">Выход</a>
            <a href="deleteSession.php" class="view__button button button--danger">Забыть сессию</a>
        </div>
    </main>
</body>
</html>