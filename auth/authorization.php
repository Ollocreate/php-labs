<?php 
session_start();

if (isset($_SESSION['UID'])) {
    header('Location: index.php');
} elseif (isset($_COOKIE['token'])) {
    $database = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_1808_neustroeva', 'neustroeva(123)', 'std_1808_neustroeva');
    
    $token = $_COOKIE['token'];
    $sql = 'SELECT `id` FROM users WHERE `token` = "' . $token . '";';
    $res = mysqli_query($database, $sql);
    $res = mysqli_fetch_assoc($res);

    if ($res) {
        $_SESSION['UID'] = $res['id'];
        setcookie('token', $token, time() + 604800);
        header('Location: index.php');
    } else {
        setcookie('token', "", time() - 3600);
    }

} else {
    ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Вход</title>
</head>
<body>
    <header></header>
    <main>
        <section class="auth">
            <div class="container">
                <form action="auth.php" class="form" method="post">
                    <h2 class="form__title">Авторизация</h2>
                    <div class="form__field">
                        <input required type="text" class="form__input" name="login" id="login" placeholder="Ваш логин">
                    </div>
                    <div class="form__field">
                        <input required type="password" class="form__input" name="password" id="password" placeholder="Ваш пароль">
                    </div>
                    <div class="form__field">
                        <input type="checkbox" name="save-me" id="save-me">
                        <label for="save-me" class="form__label">Запомнить меня</label>
                    </div>
                    <div class="form__field form__field--flex">
                        <button class="form__button button" name="auth">Войти</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <div class="notify">
        <div class="notify__message">
            <p>
                <?php
                if (isset($_SESSION['error'])) {
                    switch($_SESSION['error']) {
                        case 'data': {
                        echo 'Введенные данные некорректны.';
                        break;
                    }
                    case 'connection': {
                        echo 'Ошибка соединения с базой данных.';
                        break;
                    }
                    case 'user': {
                        echo 'Пользователя с таким Email не существует.';
                        break;
                    }
                    case 'password': {
                        echo 'Ошибка входа. Неправильный пароль.';
                        break;
                    }
                    }
                    unset($_SESSION['error']);
                }
                ?>
            </p>
        </div>
        </div>
</body>
</html>
    <?php
}
