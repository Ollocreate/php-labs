<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2 вариант</title>
</head>
<body>
    <?php
    $list = array("X + 3 = 7", "27 - X = 17", "6 / X = 2", "X / 8 = 6", "22 * X = 220", 
                       "X * 7 = 49", "10 + X = 33", "X + 67 = 129", "4 * X = 36", "X * 9 = 56");

    foreach ($list as $equation) {
        echo "_______________"."<br>";
        echo "Дано: $equation"."<br>";
        $parts = explode(" ", $equation);

        if ($parts[1] == "-") {
            $action = "вычитание";
            echo "Действие: $action"."<br>";

            if ($parts[2] == "X") {
                $role = "вычитаемое";
                echo "X - $role"."<br>";

                $X = $parts[0] - $parts[4];
                echo "X = $X"."<br>";
            } else {
                $role = "уменьшаемое";
                echo "X - $role"."<br>";

                $X = $parts[2] + $parts[4];
                echo "X = $X"."<br>";
            }

        } elseif ($parts[1] == "+") {
            $action = "сложение";
            echo "Действие: $action"."<br>";
            $role = "слагаемое";
            echo "X - $role"."<br>";

            if ($parts[0] == "X") {
                $X = $parts[4] - $parts[2];
            } else {
                $X = $parts[4] - $parts[0];
            }

            echo "X = $X"."<br>";
        } elseif ($parts[1] == "*") {
            $action = "умножение";
            echo "Действие: $action"."<br>";
            $role = "множитель";
            echo "X - $role"."<br>";

            if ($parts[0] == "X") {
                $X = $parts[4] / $parts[2];
            } else {
                $X = $parts[4] / $parts[0];
            }

            echo "X = $X"."<br>";

        }
        else {
            $action = "деление";
            echo "Действие: $action"."<br>";

            if ($parts[2] == "X") {
                $role = "делитель";
                echo "X - $role"."<br>";

                $X = $parts[0] / $parts[4];
                echo "X = $X"."<br>";
            } else {
                $role = "делимое";
                echo "X - $role"."<br>";

                $X = $parts[2] * $parts[4];
                echo "X = $X"."<br>";
            }
        }
    }    
    ?>
</body>
</html>